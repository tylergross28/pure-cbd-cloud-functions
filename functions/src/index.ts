import * as functions from 'firebase-functions';
import axios from 'axios';
import WooOrder, { CouponLine, LineItem } from './WooOrder';
import ShopifyOrder from './ShopifyOrder';

interface ApiKeys {
  shopify_api_key: string;
  shopify_api_secret: string;
  shopify_api_password: string;
}

export const orderForwarder = functions.https.onRequest(
  async (request, response) => {
    const wooOrder: WooOrder = request.body;
    try {
      const keys: ApiKeys = functions.config().keys;
      const apiKey = keys.shopify_api_key;
      const apiPassword = keys.shopify_api_password;
      const url = `https://${apiKey}:${apiPassword}@pure-cbd-exchange.myshopify.com/admin/orders.json`;
      const shopifyOrder: ShopifyOrder = {
        order: {
          billing_address: {
            address1: wooOrder.billing.address_1,
            address2: wooOrder.billing.address_2,
            city: wooOrder.billing.city,
            country: wooOrder.billing.country,
            company: wooOrder.billing.company,
            first_name: wooOrder.billing.first_name,
            last_name: wooOrder.billing.last_name,
            phone: wooOrder.billing.phone,
            province: wooOrder.billing.state,
            zip: wooOrder.billing.postcode,
            name: `${wooOrder.billing.first_name} ${wooOrder.billing.last_name}`
          },
          client_details: {
            user_agent: wooOrder.customer_user_agent,
            browser_ip: wooOrder.customer_ip_address
          },
          buyer_accepts_marketing: true,
          created_at: wooOrder.date_created,
          currency: wooOrder.currency,
          discount_codes: wooOrder.coupon_lines.map((coop: CouponLine) => ({
            code: coop.code,
            amount: coop.discount
          })),
          email: wooOrder.billing.email,
          fullfillment_status: 'Processing',
          id: wooOrder.id,
          landing_site: 'https://purecbdexchange.com',
          line_items: wooOrder.line_items.map((item: LineItem) => ({
            price: `${item.price}`,
            product_id: item.product_id,
            quantity: item.quantity,
            sku: item.sku,
            name: item.name,
            title: item.name,
            variant_id: item.variation_id,
            taxable: true,
            requires_shipping: true
          })),
          name: `${wooOrder.id}`,
          phone: wooOrder.billing.phone,
          presentment_currency: wooOrder.currency,
          suppress_notifications: true,
          financial_status: wooOrder.status === 'failed' ? 'voided' : 'paid',
          processed_at: wooOrder.date_created,
          shipping_address: {
            address1: wooOrder.shipping.address_1,
            address2: wooOrder.shipping.address_2,
            city: wooOrder.shipping.city,
            company: wooOrder.shipping.company,
            country: wooOrder.shipping.country,
            first_name: wooOrder.shipping.first_name,
            last_name: wooOrder.shipping.last_name,
            phone: wooOrder.billing.phone,
            province: wooOrder.shipping.state,
            zip: wooOrder.shipping.postcode,
            name: `${wooOrder.shipping.first_name} ${
              wooOrder.shipping.last_name
            }`
          },
          total_tax: wooOrder.total_tax,
          total_price: wooOrder.total,
          tags: 'Imported from Woocommerce',
          taxes_included: false,
          send_receipt: false,
          send_fulfillment_receipt: false,
          test: false,
          total_discounts: wooOrder.discount_total
        }
      };
      if (wooOrder.status === 'failed') {
        console.error('Failed order: ', wooOrder);
      } else {
        await axios.post(url, shopifyOrder);
      }
      return response.status(201).send();
    } catch (e) {
      console.error(e);
      console.error('Woo Order: ', wooOrder);
      return response.status(400).send();
    }
  }
);
