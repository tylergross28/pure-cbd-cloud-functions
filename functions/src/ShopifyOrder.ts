export default interface ShopifyOrder {
  order: {
    suppress_notifications: boolean;
    readonly app_id?: number;
    billing_address: Address;
    readonly browser_ip?: string;
    buyer_accepts_marketing: boolean;
    cancel_reason?: string;
    readonly cancelled_at?: string;
    readonly cart_token?: string;
    readonly client_details: ClientDetails;
    readonly closed_at?: string;
    readonly created_at?: string;
    currency: string;
    customer?: Customer;
    readonly customer_locale?: string;
    readonly discount_applications?: DiscountApplications[];
    discount_codes: DiscountCode[];
    email: string;
    financial_status?: 'pending' | 'authorized' | 'partially_paid' | 'paid' | 'partially_refunded' | 'refunded' | 'voided';
    fulfillments?: Fullfillment[];
    fullfillment_status: string;
    readonly id?: number;
    readonly landing_site?: string;
    line_items: LineItem[];
    location_id?: number;
    readonly name?: string;
    note?: string;
    note_attributes?: Properties[];
    readonly number?: number;
    readonly order_number?: number;
    readonly payment_gateway_names?: string[];
    phone: string;
    presentment_currency: string;
    processed_at: string;
    readonly processing_method?: string;
    referring_site?: string;
    readonly refunds?: Refund[];
    shipping_address: Address;
    order_status_url?: string;
    user_id?: number;
    updated_at?: string;
    total_weight?: string;
    total_tip_received?: string;
    total_tax_set?: string;
    total_tax: string;
    tax_price_set?: MoneySet;
    total_price: string;
    send_receipt: boolean;
    send_fulfillment_receipt: boolean;
    shipping_lines?: ShippingLine[];
    source_name?: string;
    subtotal_price?: number;
    subtotal_price_set?: MoneySet;
    tags: string;
    tax_lines?: TaxLine[];
    taxes_included: boolean;
    test: boolean;
    readonly token?: string;
    total_discounts: string;
    total_discounts_set?: MoneySet;
    total_line_items_price?: string;
    total_line_items_price_set?: MoneySet;
  };
}

interface Address {
  address1: string;
  address2: string;
  city: string;
  company: string;
  country: string;
  first_name: string;
  last_name: string;
  phone: string;
  province: string;
  zip: string;
  name: string;
  province_code?: string;
  country_code?: string;
  latitude?: number;
  longitude?: number;
}

interface ClientDetails {
  accept_language?: string;
  browser_height?: number;
  browser_ip: string;
  browser_width?: number;
  session_hash?: string;
  user_agent: string;
}

interface Customer {
  id: number;
  email: string;
  accepts_marketing: false;
  created_at: string;
  updated_at: string;
  first_name: string;
  last_name: string;
  orders_count: string;
  state: string;
  total_spent: string;
  last_order_id: number;
  note: null;
  verified_email: boolean;
  multipass_identifier: null;
  tax_exempt: boolean;
  phone: string;
  tags: string;
  last_order_name: string;
  currency: string;
  addresses: any;
  admin_graphql_api_id: string;
  default_address: any;
}

interface DiscountApplications {
  type: string;
  title: string;
  description: string;
  value: string;
  value_type: string;
  allocation_method: string;
  target_selection: string;
  target_type: string;
}

interface DiscountCode {
  code: string;
  amount: string;
  type?: string;
}

interface Fullfillment {
  created_at: string;
  id: number;
  order_id: number;
  status: number;
  tracking_company: null;
  tracking_number: string;
  updated_at: string;
}

interface LineItem {
  fulfillable_quantity?: number;
  fulfillment_service?: string;
  fulfillment_status?: string;
  grams?: number;
  id?: number;
  price: string;
  product_id: number;
  quantity: number;
  requires_shipping: boolean;
  sku: string;
  title?: string;
  variant_id: number;
  variant_title?: string;
  vendor?: string;
  name: string;
  gift_card?: boolean;
  price_set?: MoneySet;
  properties?: Properties[];
  taxable: boolean;
  tax_lines?: TaxLine[];
  total_discount?: string;
  total_discount_set?: MoneySet;
  discount_allocations?: DiscountApplications[];
}

interface Money {
  amount: string;
  currency_code: string;
}

interface Properties {
  name: string;
  value: string;
}

interface MoneySet {
  shop_money: Money;
  presentment_money: Money;
}

interface TaxLine {
  title: string;
  price: string;
  price_set: MoneySet;
  rate: number;
}

interface Refund {
  id: number;
  order_id: number;
  created_at: string;
  note: string;
  user_id: string;
  processed_at: string;
  refund_line_items: LineItem[];
  transactions: any[];
  order_adjustments: any[];
}

interface ShippingLine {
  code: string;
  price: string;
  price_set: MoneySet;
  discounted_price: string;
  discounted_price_set: string;
  source: string;
  title: string;
  tax_lines: TaxLine[];
  carrier_identifier: string;
  requested_fulfillment_service_id: string;
}
